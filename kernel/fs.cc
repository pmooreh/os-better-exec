#include "fs.h"

size_t File::readAll(size_t offset, void* buf, size_t nbyte) {
    size_t togo = nbyte;
    char* ptr = (char*)buf;

    while (togo > 0) {
        size_t n = read(offset,ptr,togo);
        if (n == 0) break;
        if (n > togo) Debug::panic("%d > %d",n,togo);
        togo -= n;
        ptr += n;
        offset += n;
    }
    return nbyte - togo;
}

