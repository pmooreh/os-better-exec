#include "string.h"

int String::strlen(const char* in) {
    int i=0;
    const char* p = in;

    while (p[i] != 0) i++;
    return i;    
}

char* String::strdup(const char* in) {
    int n = strlen(in);
    char* p = new char[n+1];
    for (int i=0; i<=n; i++) {
        p[i] = in[i];
    }
    return p;
}
