#include "u8250.h"
#include "machine.h"
#include "thread.h"

/* 8250 */

void U8250::put(char c) {
    while (!(inb(0x3F8+5) & 0x20));
    outb(0x3F8,c);
}

char U8250::get() {
    getLock.lock();
    while (!(inb(0x3F8+5) & 0x01)) {
       Thread::yield();
    }
    char x = inb(0x3F8);
    getLock.unlock();
    return x;
}
