#include "sys.h"
#include "stdint.h"
#include "idt.h"
#include "debug.h"
#include "thread.h"
#include "locks.h"
#include "kernel.h"
#include "elf.h"

StrongPtr<Event>* SYS::eventArr;
BlockingLock* SYS::lock;

struct SwitchParam {
    uint32_t pc;
    uint32_t esp;
    SwitchParam(uint32_t pc, uint32_t esp)
        : pc(pc), esp(esp) {}
    ~SwitchParam() {}
};

void forkSwitch(SwitchParam param) {
    switchToUser(param.pc, param.esp, 0);
}

int divideAndCeil(int dividend, int divisor) {
    int ret = dividend / divisor;
    if (dividend % divisor != 0)
        ret++;
    return ret;
}

int getStringLength(char* str) {
    int len = 0;

    while (str[len] != 0)
        len++;

    return len + 1;
}

/*
    my research shows that magic[3] has the stack frame
    we want, and stack[1] has the variable we want! yay

number   signature                        description
0        void exit();                     terminate the current thread
1        write
2        int id= fork()                   returns twice
                                              id == 0 --> child
                                              id != 0 --> parent 
3        int id = semaphore(int count)    creates a semaphore with the
                                          given count
4        void up(int id)                  up
5        void down(int id)                down
6        void shutdown()                  shutdown
7        void join(int id)                block until the given process

8        open file
9        close file
10       read
11       length
12       seek
13       exec

*/
extern "C" int sysHandler(uint32_t eax, uint32_t *magic) {
    uint32_t* userStack = (uint32_t*)magic[3];
    uint32_t arg = userStack[1];
    uint32_t arg1 = userStack[2];
    uint32_t arg2 = userStack[3];
    switch (eax) {
        case 0: {
            Thread::exit();
			return 0;
			break;
}
        case 1: {
            // write
            int fd = (int) arg;
            void* buf = (void*) arg1;
            size_t nbyte = (size_t) arg2;
            if (fd < 0) return -1;
            if (fd >= Process::NFILES) return -1;
            StrongPtr<Process> me = Thread::current()->process;
            StrongPtr<OpenFile> of = me->files[fd];
            if (of.isNull()) return -1;
            return of->write(buf,nbyte);
			break;					/* XXX security */
        }
        case 2: {
			// lock it down!!
			SYS::lock->lock();

			SwitchParam param = SwitchParam(magic[0], magic[3]);
            // create new process
            StrongPtr<Process> child = StrongPtr<Process>(new Process());
            // copy the address space
			Thread::current()->process->addressSpace->fork(child->addressSpace);
			// set files and sech same for parents and kiddos
            child->setSharedInfo(Thread::current()->process);
            // create new thread
            Thread* cThread = new FuncThread<SwitchParam>(child, forkSwitch, param);
            // now add child's thread's exitEvent; will serve as the id of the child
			int cId = cThread->id;
            SYS::eventArr[cId] = cThread->exitEvent;
            // make this thread run!!!
            Thread::start(cThread);
			// Debug::printf("id: %d\n", cId);
			
			// unlock !!
			SYS::lock->unlock();

			return cId;
			break;
        }
        case 3: {
            int id = Thread::current()->process->addSemaphore(arg);
            return id;
			break;
        }
        case 4: {
			Thread::current()->process->getSemaphore(arg)->up();
			return 0;
			break;
        }
        case 5: {
			Thread::current()->process->getSemaphore(arg)->down();
			return 0;
			break;
        }
        case 6: {
            Debug::panic("shut 'er down");
            return 0;
			break;
        }
        case 7: {
            // Debug::printf("    wait on Process with id: %d\n", arg);
            SYS::eventArr[arg]->wait();
            SYS::eventArr[arg].reset();
            return 0;
			break;
        }
        case 8: {
            // open file!

			// first, get the name. 
			// int len = getStringLength((char*)arg);
			// char* fileName = new char[len];
			// memcpy((void*)fileName, (void*)arg, len);
			
			StrongPtr<File> file = kernelInfo->rootDir->lookupFile((char*)arg);

			// delete[] fileName;

            if (file.isNull()) {
                return -1;
			}

            int fileDescriptor = Thread::current()->process->addFile(file);

            return fileDescriptor;
			break;
        }
        case 9: {
            // close file!
            Thread::current()->process->closeFile(arg);
            return 0;
			break;
        }
        case 10: {
            // read! 3 args this time
            // ssize_t read(int fd, void* buf, size_t nbyte)
            
            //Debug::printf("arg0: %d, arg1: %x, arg2: %d\n", arg, arg1, arg2);
            
            return Thread::current()->process->readFile(arg, (void*)arg1, (size_t)arg2);
			break;
		}
        case 11: {
            // length
            return Thread::current()->process->getFileLength(arg);
			break;
		}
        case 12: {
            // size_t seek(int fd, size_t offset)
            return Thread::current()->process->seekFile(arg, arg1);
			break;
        }
        case 13: {
            //exec!

            StrongPtr<File> file = kernelInfo->rootDir->lookupFile((char*)arg);

            if (file.isNull()) {
                return -1;
            }

            int numArgs = arg1;
            char** argArr = new char*[numArgs];
            int* argLengthArr = new int[numArgs];

            for (int i = 0; i < numArgs; i++) {
                int argLen = getStringLength((char*)(userStack[i + 3]));
                argLengthArr[i] = argLen;
                argArr[i] = new char[argLen];
                memcpy((void*)argArr[i], (void*)userStack[i + 3], argLen);
                // Debug::printf("arg number %d: %s\n", i, argArr[i]);
            }

            // Debug::printf("stack is where? %x\n", magic[3]);
            // Debug::printf("stack according to thread? %x\n", Thread::current()->esp);

            // at this point: get rid of the address space,
            // then execute the new file

            StrongPtr<AddressSpace> newSpace = StrongPtr<AddressSpace>(new AddressSpace());
            newSpace->activate();
            Thread::current()->process->addressSpace = newSpace;
            newSpace.reset();
   
            uint32_t e = ELF::load(file);

            // Debug::printf("stack is where? %x\n", magic[3]);
            // Debug::printf("stack according to thread? %x\n", Thread::current()->esp);

            // add everything to the stack at 0xfffff000

            uint32_t newEsp = 0xfffff000;

            uint32_t* argAddrs = new uint32_t[numArgs];

            // put char literals on string
            for (int i = 0; i < numArgs; i++) {
                int subtract = 4 * divideAndCeil(argLengthArr[numArgs - 1 - i], 4);

                newEsp = newEsp - subtract;
                argAddrs[i] = newEsp;
                memcpy((void*)newEsp, (void*)argArr[numArgs - 1 - i], argLengthArr[numArgs - 1 - i]);
            }

            // for (int i = 0; i < 30; i++) {
            //     Debug::printf("%x: %c\n", newEsp + i, ((char*)newEsp)[i]);
            // }

            // put the strings on the stack
            for (int i = 0; i < numArgs; i++) {
                newEsp -= 4;
                ((uint32_t*)newEsp)[0] = argAddrs[i];
            }

            // put pointer to pointers
            newEsp -= 4;
            ((uint32_t*)newEsp)[0] = newEsp + 4;

            // finally put num args
            newEsp -= 4;
            ((uint32_t*)newEsp)[0] = (uint32_t)numArgs;


            // for (int i = 0; i < numArgs + 2; i++) {
            //     Debug::printf("index %d, address %x: %x\n", i, newEsp + 4*i, ((uint32_t*)newEsp)[i]);
            // }

            // get rid of all arrays
            // strings, then char**, then length, then addresses
            for (int i = 0; i < numArgs; i++) {
                delete[] argArr[i];
            }

            delete[] argArr;
            delete[] argLengthArr;
            delete[] argAddrs;

            switchToUser(e, newEsp, 0x12345678);
            return 0;
			break;
        }
        default:
            Debug::panic("unknown system call %d",eax);
            return 0;
			break;
    }
    
}

extern "C" void sysAsmHandler();

void SYS::init(void) {
    IDT::trap(48,(uint32_t)sysAsmHandler,3);

    eventArr = new StrongPtr<Event>[2020];
	lock = new BlockingLock();
}


