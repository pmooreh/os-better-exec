#ifndef _MACHINE_H_
#define _MACHINE_H_

#include "stdint.h"

extern "C" void switchToUser(uint32_t pc, uint32_t esp, uint32_t eax);

extern uint32_t kernelCS;
extern uint32_t kernelUserDS;
extern uint32_t kernelSS;
extern uint32_t kernelCS;

extern uint32_t tssEsp0;

extern "C" void invlpg(uint32_t va);
extern "C" void vmm_on(uint32_t pd);
extern "C" void memcpy(void* dest, const void* src, size_t n);
extern "C" void bzero(void* dest, size_t n);
extern "C" int getThenIncrement(volatile int* ptr, int d);

extern "C" uint32_t cli(void);
extern "C" uint32_t sti(void);

extern "C" int inb(int port);
extern "C" int inl(int port);
extern "C" void outb(int port, int val);

extern "C" void pit_do_init(uint32_t d);

extern "C" void ltr(uint32_t tr);

extern "C" void pageFaultHandler();

extern "C" uint32_t getcr0();
extern "C" uint32_t getcr3();
extern "C" void invlpg(uint32_t);

extern "C" void pitAsmHandler(void);
extern "C" void pic0(void);
extern "C" void pic1(void);
extern "C" void pic2(void);
extern "C" void pic3(void);
extern "C" void pic4(void);
extern "C" void pic5(void);
extern "C" void pic6(void);
extern "C" void pic8(void);
extern "C" void pic7(void);
extern "C" void pic8(void);
extern "C" void pic9(void);
extern "C" void pic10(void);
extern "C" void pic11(void);
extern "C" void pic12(void);
extern "C" void pic13(void);
extern "C" void pic14(void);
extern "C" void pic15(void);

#endif
