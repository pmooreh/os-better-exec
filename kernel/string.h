#ifndef _STRING_H_
#define _STRING_H_

class String {
public:
    char* it;
    static char* strdup(const char* in);
    static int strlen(const char* in);

    String(char *from) : it(strdup(from)) {}
    ~String() {
        if (it != nullptr) {
            delete []it;
            it = nullptr;
        }
    }
};

#endif
