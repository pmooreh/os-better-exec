#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include "refs.h"
#include "locks.h"

class SYS {
public:
    static void init(void);
    static StrongPtr<Event>* eventArr;
	static BlockingLock* lock;
};



#endif
