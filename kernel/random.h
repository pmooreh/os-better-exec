#ifndef _RANDOM_H_
#define _RANDOM_H_

#include "stdint.h"

extern void randomInit(uint32_t seed);
extern uint32_t random();

#endif
