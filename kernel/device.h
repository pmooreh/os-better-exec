#ifndef _DEVICE_H_
#define _DEVICE_H_

#include "stdint.h"
#include "refs.h"
#include "locks.h"

class Event;


/******************************/
/* Base class for all devices */
/******************************/

class Device {
public:
     /* Every device in the system has a unique id, more on that later */
     const uint32_t id;

     Device(uint32_t id) : id(id) {}
};

struct BlockBuffer;

/********************************/
/* Operations for block devices */
/********************************/

class BlockIO {
public:
    /* Block size is always 4K */
    static constexpr size_t BLOCK_SIZE = 4096;

    virtual ~BlockIO() {}

    /* Non-blocking read. Returns immediately and the ready event is
       signalled when the read completes */
    virtual StrongPtr<BlockBuffer> readBlock(size_t blockNumber) = 0;
    
    /* read up to nbyte starting at offset, never crosses block bounderies */
    /* returns the number of bytes actually read */
    virtual size_t read(size_t offset, void* buf, size_t nbyte);

    /* read nbyte starting at offset */
    /* returns nbyte unless an error occured */
    virtual size_t readAll(size_t offset, void* buf, size_t nbyte);
};


/* A block device is a device that implements the block oeprations */

class BlockDevice : public Device, public BlockIO {
public:

    BlockDevice(uint32_t id) : Device(id) {
    }

    virtual ~BlockDevice() {}

};

/* Returned by BlockIO::readBlock */
struct BlockBuffer {
    /* signalled when the read completes */
    StrongPtr<Event> ready;

    /* The data, not valid until ready is signalled */
    char* data;

    /* The device that produced the block */
    const uint32_t id;

    /* Block number in the device */
    const uint32_t blockNum;

    StrongPtr<BlockBuffer> next;

    BlockBuffer(uint32_t id, uint32_t blockNum);
    ~BlockBuffer();
};

#endif
