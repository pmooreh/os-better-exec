#ifndef _IDE_H_
#define _IDE_H_

#include "device.h"
#include "refs.h"

class IDE : public BlockDevice {
public:
    IDE(uint32_t id);

    /* From BlockIO */
    virtual StrongPtr<BlockBuffer> readBlock(size_t blockNumber);

    static void init(void);

    static StrongPtr<BlockIO> a();
    static StrongPtr<BlockIO> b();
    static StrongPtr<BlockIO> c();
    static StrongPtr<BlockIO> d();

};

#endif
