#include "libc.h"
#include "sys.h"

#define NULL 0
char *cmd = NULL;

void go() {
    if (cmd == NULL) {
        putstr("*** Command not given\n");
    }
    putstr("*** $ ");
    putstr(cmd);
    putstr("\n");
//putstr("forking\n");
    int id = fork();
    if (id == 0) {
//putstr("calling exec\n");
        int ret = exec(cmd,0);
        if (ret == -1) {
            putstr("*** Command not found\n");
            exit(0);
        }
        else {
            putstr("*** Weird things happened\n");
            exit(0);
        }
    }
    else {
//putstr("joining\n");
        join(id);
//putstr("joined\n");
        return;
    }
}

int main() {
    cmd = "echo";
    go();
    cmd = "aoeuidht";
    go();
    return 0;
}
