#include "libc.h"

int main() {
    putstr("*** hello\n");

    int sem = semaphore(0);

    putstr("*** open big\n");
    int fd = open("big");
    SHOW(fd);

    size_t off = seek(fd,40);
    SHOW(off);

    char buf[8];


    readAll(fd,buf,8);
    buf[7] = 0;
    saystr(buf);

    int id = fork();
    if (id == 0) {
        /* child */
        readAll(fd,buf,8);
        buf[7] = 0;
        saystr(buf);
        up(sem);
    } else {
        down(sem);
        readAll(fd,buf,8);
        buf[7] = 0;
        saystr(buf);
    }

    return 0;
}
