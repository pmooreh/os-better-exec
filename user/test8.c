#include  "libc.h"

#define N 1000

typedef enum { false, true } bool;

int buf[N];
int x;

void setData() {
	    for (int i = 0; i < N; i++) {
			        buf[i] = 2 * i - i;
					    }
		    x = 0x12345678;
}

bool checkData() {
	    bool bufSame = true;
		    for (int i = 0; i < N; i++) {
				        if (buf[i] != 2 * i - i) {
							            bufSame = false;
										            break;
													        }
						    }
			    bool xSame = (x == 0x12345678);
				    return (bufSame || xSame);
}

int main(int argc, char** argv) {
	    saystr("hello");
		    if (argc == 0) {
				        setData();
						        exec("test8", 2, "test8", "done");
								    } else if (argc == 2) {
										        if (!checkData()) {
													            saystr(argv[1]);
																            return 0;		
																			        } else {
																						            return -1;
																									        }
												    } else {
														        return -1;
																    }
}
