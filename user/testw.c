#include "libc.h"

#define N   2000
#define LEN 5

int atoi(const char* str) {
	    int val = 0; 
		    for (int i = 0; str[i] != '\0'; i++)
				        val = val * 10 + (str[i] - '0');
			    return val;
}

void reverse(char* str, int n) {
	    char temp;
		    for (int i = 0; i < n / 2; i++) {
				        temp = str[i];
						        str[i] = str[n - i - 1];
								        str[n - i - 1] = temp;
										    }
}

void itoa(int val, char* buf) {
	    int ind = 0;
		    int digit;
			    while (val > 0) {
					        digit = val % 10;
							        buf[ind++] = digit + '0';
									        val /= 10;
											    }
				    reverse(buf, ind);
					    buf[ind] = '\0';
}

static char s[LEN];
int main(int argc, char** argv) {
	    if (argc == 0) {
			        itoa(N, s);
					        exec("testw", 2, "testw", s);
							    } else if (argc == 2) {
									        int n = atoi(argv[1]);
											        if ((n % 100) == 0) saydec("", n);
													        if (n == 0) return 0;
															        itoa(n - 1, s);
																	        exec("testw", 2, "testw", s);
																			    } else {
																					        return -1;
																							    }
}
