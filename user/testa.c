#include "libc.h"

int main() {
    putstr("*** hello\n");


    putstr("*** open hello.txt\n");
    int fd = open("hello.txt");
    SHOW(fd);

    size_t n = len(fd);
    SHOW(n);

    char buf[100];
    buf[n] = 0;
    for (int i=0; i<=n; i++) {
        ssize_t x = read(fd,&buf[i],1);
        SHOW(x);
    }
    saystr(buf);


    close(fd);

    return 0;
}
