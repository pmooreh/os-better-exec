#include "libc.h"

int main() {
    putstr("*** hello\n");

    char* args[5];

    args[0] = "echo";
    args[1] = "hello";
    args[2] = "there";
    args[3] = "execv";
    args[4] = 0;

    int rc = execv("echo",args);

    saydec("bad",rc);

    return 0;
}
